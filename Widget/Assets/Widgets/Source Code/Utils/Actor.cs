﻿using UnityEngine;
using System.Collections;

namespace eeGames.Actor
{
    public enum LoopType
    {
        PingPong,
        StartOver
    }
    // 
    [System.Serializable]
    public class Actor
    {
        public ActorData PositionActor;
        public ActorData RotationActor;
        public ActorData ScaleActor;
        public ActorData ColorActor;


        public void OnStartPositionActing() { }
        public void OnStartRotationActing(GameObject obj) 
        {
            if (!ScaleActor.IsActive) return;
            var mainWindow = obj.GetComponent<RectTransform>();
            mainWindow.transform.localScale = ScaleActor.From;
            LTDescr id = LeanTween.scale(mainWindow, ScaleActor.To, ScaleActor.Time).setEase(ScaleActor.TweenType);

        }
        public void OnStartScaleActing() { }
        public void OnStartColorActing() { }

        public void OnEndPositionActing() { }
        public void OnEndRotationActing() { }
        public void OnEndScaleActing() { }
        public void OnEndColorActing() { }



        public void OnStartActing(GameObject obj) 
        {
            OnStartRotationActing(obj);
        }
        public void OnEndActing() 
        {
 
        }

    }

    /// <summary>
    /// data structure used to store tween data
    /// </summary>
    [System.Serializable]
    public struct ActorData
    {
        public bool IsActive;
        public float Time;
        public float DelayTime;
        public int TweenCount;
        public bool IsAutoPlay;
        public bool IsLoop;
        public LeanTweenType TweenType;
        public LoopType LoopType;
        public Vector4 From;
        public Vector4 To;
        public Vector4 Hide;
    }


}