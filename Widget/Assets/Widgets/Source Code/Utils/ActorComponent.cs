﻿using UnityEngine;
using System.Collections;

namespace eeGames.Actor
{
    public class ActorComponent : MonoBehaviour
    {
        public ActorEvent OnStart;
        public ActorEvent OnStop;
        [HideInInspector] public Actor Actor; 
        // Use this for initialization
        void Start()
        {

        }

       
    }


    [System.Serializable]
    public class ActorEvent : UnityEngine.Events.UnityEvent
    { }
}