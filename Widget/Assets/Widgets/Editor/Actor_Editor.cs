﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.AnimatedValues;
using eeGames.Widget;
using System.Collections.Generic;
using eeGames.Actor;


[CustomEditor(typeof(ActorComponent), true), CanEditMultipleObjects]
public class Actor_Editor : Editor
{
    private ActorComponent Target { get { return (ActorComponent)target; } }
    private AnimBool networkingFoldout;
    private bool m_tweens;
    private GUIStyle m_editorStyle;
    private ActorVec3Editor m_actor;

    private List<CommonActorElements> m_actorElements = new List<CommonActorElements>();
    private void OnEnable()
    {
        m_actorElements.Clear();
        networkingFoldout = new AnimBool(true);

        m_editorStyle = new GUIStyle();
        m_editorStyle.normal.textColor = Color.white;
        m_editorStyle.fontStyle = FontStyle.Bold;
        m_editorStyle.alignment = TextAnchor.UpperCenter;


        var dummy = Target;
        if (dummy.Actor == null)
        {
            dummy.Actor = new Actor();
              Debug.Log("Hey Target is null");
        }


        m_actor = new ActorVec3Editor();
        var sActorData = Target.Actor.ScaleActor;
        m_actor.Init("Scale Actor", TweenType.SCALE, sActorData.Time, sActorData.DelayTime, sActorData.IsActive,
            sActorData.IsAutoPlay, sActorData.IsLoop, sActorData.TweenType, sActorData.LoopType, sActorData.TweenCount,
            sActorData.To, sActorData.From, sActorData.Hide,
            (val) =>
            {
                sActorData.Time = val;
            },
            (val) =>
            {
                sActorData.DelayTime = val;
            },
            (val) =>
            {
                sActorData.IsActive = val;
            },
            (val) =>
            {
                sActorData.IsAutoPlay = val;
            },
            (val) =>
            {
                sActorData.IsLoop = val;
            },
            (val) =>
            {
                sActorData.TweenType = val;
            },
            (val) =>
            {
                sActorData.LoopType = val;
            },
            (val) =>
            {
                sActorData.TweenCount = val;
            },
            (val) =>
            {
                sActorData.To = val;
            },
            (val) =>
            {
                sActorData.From = val;
            },
            (val) => 
            {
                sActorData.Hide = val;
            });
       

        m_actorElements.Add(m_actor);

  //      ActorColor colorActor = new ActorColor();
       // m_actorElements.Add(colorActor);
    }

    private static bool _positionFold = false;
    public override void OnInspectorGUI()
    {

        


        foreach (var item in m_actorElements)
        {
            item.Update();
            if (item.Enable) DrawDefaultInspector();
        }
//        EditorGUILayout.Space();
 //       EditorGUILayout.Separator();
 ////       EditorGUILayout.Space();

 //       Rect area = GUILayoutUtility.GetRect(0.0f, 30.0f, GUILayout.ExpandWidth(true));

 //       if (m_tweens == false)
 //       {
 //           //GUI.color = Color.cyan;
           
 //           EditorGUILayout.HelpBox("Add WidgetTween Component For Tweens related Stuff by pressing Button Below", MessageType.Info, true);
 //      //     GUI.Button(Rect(0,0,200,20), "enable");

 //           //GUI.skin. = FontStyle.Bold;
 //           _positionFold = EditorGUILayout.Foldout(_positionFold, "Position Tween");
 //           EditorGUILayout.BeginVertical(GUI.skin.box,GUILayout.ExpandWidth(true));
 //           //Rect area2 = GUILayoutUtility.GetRect(30f, 200.0f, GUILayout.ExpandWidth(true));
 //           //GUI.Box(area2, "Add Tween");
 //           GUI.backgroundColor = Color.cyan;

 //           if(_positionFold)
 //           {
 //               EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
 //               GUILayout.Button("To", GUILayout.Width(75));
 //               EditorGUILayout.Vector3Field("", Vector3.back, GUILayout.ExpandWidth(true));
 //               EditorGUILayout.EndHorizontal();



 //               EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
 //               GUILayout.Button("To", GUILayout.Width(75));
 //               EditorGUILayout.Vector3Field("", Vector3.back, GUILayout.ExpandWidth(true));
 //               EditorGUILayout.EndHorizontal();



 //               EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
 //               GUILayout.Button("To", GUILayout.Width(75));
 //               EditorGUILayout.Vector3Field("", Vector3.back, GUILayout.ExpandWidth(true));
 //               EditorGUILayout.EndHorizontal();
 //           }
           


 //           EditorGUILayout.EndVertical();




 //           EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
 //           //Rect area2 = GUILayoutUtility.GetRect(30f, 200.0f, GUILayout.ExpandWidth(true));
 //           //GUI.Box(area2, "Add Tween");

 //           EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
 //           GUILayout.Button("From", GUILayout.Width(75));
 //           EditorGUILayout.Vector3Field("", Vector3.back, GUILayout.ExpandWidth(true));
 //           EditorGUILayout.EndHorizontal();

 //           EditorGUILayout.EndVertical();




 //           EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
 //           //Rect area2 = GUILayoutUtility.GetRect(30f, 200.0f, GUILayout.ExpandWidth(true));
 //           //GUI.Box(area2, "Add Tween");

 //           EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
 //           GUILayout.Button("Hide", GUILayout.Width(75));
 //           EditorGUILayout.Vector3Field("", Vector3.back, GUILayout.ExpandWidth(true));
 //           EditorGUILayout.EndHorizontal();

 //           EditorGUILayout.EndVertical();
       
 //           GUI.color = Color.white;
 //           EditorUtility.SetDirty(this);
 //       }
        
       


 //       // Needed because the enum's keep getting reset
        EditorUtility.SetDirty(Target);

    

        Repaint();
    }
}
